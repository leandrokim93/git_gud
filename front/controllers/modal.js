var modalVueApp = new Vue({
    el: '#repositoryModal',
    data: {
        key: "value"
    },
    methods: {
        closeRepositoryModal: function (event) {
            closeRepoModal();
        },
        openRepository: function (event) {
            document.getElementById('inputFile').click();
        }
    }
});