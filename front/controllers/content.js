var contentVueApp = new Vue({
    el: '#contentVueApp',
    data: {
        navBarResizing: false,
        descriptionBarResizing: false,
        projectOpened: false,
        logData: [],
        gitTree: [],
        hashTable: {},
        branchData: {}
    },
    methods: {
        navBarStartResize: function (event) {
            event.preventDefault();
            this.navBarResizing = true;
        },
        descriptionBarStartResize: function (event) {
            event.preventDefault();
            this.descriptionBarResizing = true;
        },
        barStopResize: function (event) {
            if (this.navBarResizing) {
                event.preventDefault();
                this.navBarResizing = false;
            }
            if (this.descriptionBarResizing) {
                event.preventDefault();
                this.descriptionBarResizing = false;
            }
        },
        barResizeUpdate: function (event) {
            if (this.navBarResizing) {
                event.preventDefault();
                if (event.clientX > 195 && event.clientX < 395) {
                    document.getElementsByClassName("navigation_content")[0].style.width = event.clientX + "px";
                }
            }
            if (this.descriptionBarResizing) {
                event.preventDefault();
                let cursorPositionFromRight = window.innerWidth - event.clientX;
                if (cursorPositionFromRight > 395 && cursorPositionFromRight < 595) {
                    document.getElementsByClassName("description_content")[0].style.width = cursorPositionFromRight + "px";
                }
            }
        },
        showRepositoryModal: function (event) {
            showRepoModal();
        }
    }
});