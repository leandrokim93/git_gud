function getfolder(e) {
    var files = e.target.files;
    var path = files[0].path;
    gitModule.openRepo(path);
}

function showRepoModal() {
    document.getElementById('repositoryModal').style.display = "block";
}

function closeRepoModal() {
    document.getElementById('repositoryModal').style.display = "none";
}

function startRepoInit() {
    gitModule.checkIsRepo(function (bool) {
        if (!bool) {
            createErrorToast('There is not a valid repository in this directory: ' + gitModule.repository._baseDir);
        } else {
            gitModule.getBranches((branches) => {
                contentVueApp.branchData = branches;
                gitModule.getLog((log) => {
                    contentVueApp.logData = log.all;
                    updateContentTree();
                    contentVueApp.projectOpened = true;
                    closeRepoModal();
                }, (error) => {
                    createErrorToast(error);
                });
            }, (error) => {
                createErrorToast(error);
            });
        }
    });
}

function createSuccessToast(message) {
    VanillaToasts.create({
        text: message,
        type: 'success',
        timeout: 5000
    });
}

function createErrorToast(message) {
    VanillaToasts.create({
        title: 'Error:',
        text: message,
        type: 'error',
        timeout: 5000
    });
}

function updateContentTree() {
    if (contentVueApp.logData.length < 1) {
        createErrorToast("No log data to convert to tree");
        return;
    }
    // console.log(contentVueApp.gitTree, contentVueApp.hashTable);
    let logLength = contentVueApp.logData.length;
    let columnLevel = [];
    for (let i = 0; i < logLength; i++) {
        let node = contentVueApp.logData[i];
        node = { ...node, ...contentVueApp.hashTable[node.hash] };
        node.parents = node.parent.split(' ');
        if (!verifyInteger(node.column)) {
            let column = -1;
            for (let b = 0; b < columnLevel.length; b++) {
                let lastNodeFromColumn = columnLevel[b][columnLevel[b].length - 1];
                if (!lastNodeFromColumn || lastNodeFromColumn.closed) { //assign closed when parent merges in another branch
                    column = b;
                }
            }
            if (column > -1) {
                node.column = column;
            } else {
                columnLevel.push([]);
                node.column = columnLevel.length - 1;
            }
            columnLevel[node.column].push(node);
        }

        for (let p = 0; p < node.parents.length; p++) {
            let hashParent = contentVueApp.hashTable[node.parents[p]];
            let parentNode = hashParent ? hashParent : {};
            if (!(verifyInteger(parentNode.column) && node.column > parent.column)) {
                // on multiple parents, from 0 column number, start assigning column
                // columnLevel[]
                // clear from branches
                parentNode.column = node.column;
                // reassign branch
            }
        }
        contentVueApp.gitTree.push(node);
        contentVueApp.hashTable[node.hash] = node;
    }
    console.log(contentVueApp);
}

function verifyInteger(int) {
    return typeof (int) == "number" && int > -1;
}