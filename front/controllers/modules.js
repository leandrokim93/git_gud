const gitModule = {
    repository: {},
    openRepo: function (path) {
        this.repository = require('simple-git')(path);
        startRepoInit();
    },
    checkIsRepo: function (callback) {
        this.repository.checkIsRepo(function (err, bool) {
            if (err) {
                return callback(false);
            }
            callback(bool);
        });
    },
    getBranches: function (callback, error) {
        this.repository.branch(function (err, data) {
            if (err) {
                return error(err);
            }
            callback(data);
        });
    },
    getLog: function (callback, error) {
        this.repository.log({
            format: {
                hash: '%h',
                date: '%ai',
                message: '%s%d',
                author_name: '%aN',
                author_email: '%ae',
                parent: '%p'
            }
        }, function (err, data) {
            if (err) {
                return error(err);
            }
            callback(data);
        });
    }
};



// eqs.fetch(function (err, fetchSummary) {
//   console.log(err, fetchSummary);
// });

// eqs.getRemotes(true, function (err, asd) {
//   console.log(err, asd);
// });

// eqs.branch(function (err, asd) {
//   console.log(err, asd);
// });

// eqs.listRemote(['--heads'], function (err, asd) {
//   console.log(err, asd);
// });

// eqs.log({ from: "cf05f0be3d0372c98d6bcf76971ae991ed5fb5c7", to: "1d89ab1d71bf4c04da03afd00b2f076816129e2f" }, function (err, asd) {
//   console.log(err, asd);
// });
