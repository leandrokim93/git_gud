var windowBarVueApp = new Vue({
    el: '#windowBarVueApp',
    data: {
        originalWidth: 0,
        originalHeight: 0,
        originalX: 0,
        originalY: 0
    },
    methods: {
        maximizeWindow: function (event) {
            if (window.outerWidth == screen.width && window.outerHeight + window.screenTop == screen.height) {
                window.resizeTo(this.originalWidth, this.originalHeight);
                window.moveTo(this.originalX, this.originalY);
            } else {
                this.originalWidth = window.outerWidth;
                this.originalHeight = window.outerHeight;
                this.originalX = window.screenX;
                this.originalY = window.screenY;
                window.moveTo(0, 0);
                window.resizeTo(screen.width, screen.height);
            }
        }
    }
});