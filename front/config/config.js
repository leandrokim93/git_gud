const config = {
    format: {
        hash: '%H',
        date: '%ai',
        message: '%s%d',
        author_name: '%aN',
        author_email: '%ae',
        parent: '%p'
    }
};