"use strict";

const {
	app,
	BrowserWindow
} = require('electron');

let mainWindow;

const options = {
	width: 1024,
	height: 600,
	minHeight: 600,
	minWidth: 1024,
	titleBarStyle: "hidden"
};

if(process.platform == "linux") {
	options.frame = false;
}

function createWindow() {
	mainWindow = new BrowserWindow(options);

	mainWindow.loadFile('./front/index.html');
	mainWindow.on('closed', function() {
		mainWindow = null;
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', function() {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function() {
	if (mainWindow === null) {
		createWindow();
	}
});
